pragma solidity >=0.6.0 <0.8.0;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/solc-0.6/contracts/access/Ownable.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/solc-0.6/contracts/math/SafeMath.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/solc-0.6/contracts/utils/Address.sol";

interface IERC271Token {
    function balanceOf(address owner) external view returns (uint256 balance);
    function name() external returns (string memory);
    function owner() external returns (string memory);
    function ownerOf(uint256 tokenId) external view returns (address);
    function getApproved(uint256 tokenId) external view returns (address operator);
    function transferFrom(address from, address to, uint256 tokenId) external;
}

// refereance contract https://github.com/martinzugnoni/auction-engine/blob/master/contracts/AuctionEngine.sol


contract AuctionEngine { 
    
    using SafeMath for uint256;
    using Address for address;
    
    event AuctionCreated(uint256 _index, address _creator, address _asset);
    event AuctionBid(uint256 _index, address _bidder, uint256 amount);
    event Claim(uint256 auctionIndex, address claimer);
    
    
    enum Status { pending, active, finished }
    
    IERC271Token public asset;
    
      struct Auction {
        address assetAddress;
        uint256 assetId;
        address  payable creator;
        uint256 startTime;
        uint256 duration;
        uint256 currentBidAmount;
        address payable currentBidOwner;
        uint256 bidCount;

        }
        
        Auction[] private auctions;
    
    constructor (       
        // address _tokencontract
        ) 
        public
        {
        // token = IERC271Token(_tokencontract);
    
    }
    
    
function createAuction (
                        address _assetAddress,
                        uint256 _assetId,
                        uint256 _startPrice,
                        uint256 _startTime,
                        uint256 _duration) 
                        public 
                        returns (uint256) {

        require(_assetAddress.isContract(), "Not a contract");
        asset = IERC271Token(_assetAddress);
        require(asset.ownerOf(_assetId) == msg.sender, "caller must be owner of teh token asset");
        require(asset.getApproved(_assetId) == address(this), "The auction contract must be apporved to transfer the token after auction");


        if (_startTime == 0) { _startTime = block.timestamp; }

        Auction memory auction = Auction({
            creator: msg.sender,
            assetAddress: _assetAddress,
            assetId: _assetId,
            startTime: _startTime,
            duration: _duration,
            currentBidAmount: _startPrice,
            currentBidOwner: address(0),
            bidCount: 0
        });
        
         auctions.push(auction);
         uint256 index = auctions.length; // not sure if i have to do a - 1
         
        emit AuctionCreated(index, auction.creator, auction.assetAddress);

        return index;
    }
    
    
    function bid(uint256 auctionIndex, uint256 amount)payable public  {
        Auction storage auction = auctions[auctionIndex];
        require(auction.creator != address(0));
        require(isActive(auctionIndex));
        require(amount == msg.value);
        require(amount > auction.currentBidAmount);
        

            if  (auction.currentBidOwner != address(0)) {
                // return funds to the previuos bidder
                //token.transfer(
                //    auction.currentBidOwner,
                //    auction.currentBidAmount
                //);
                auction.currentBidOwner.transfer(auction.currentBidAmount);
            }
            // register new bidder
            auction.currentBidAmount = amount;
            auction.currentBidOwner = msg.sender;
            auction.bidCount = auction.bidCount.add(1);

            emit AuctionBid(auctionIndex, msg.sender, amount);
          //  return true;
        
        //return false;
    }
    
    
    
    function getTotalAuctions() public view returns (uint256) { return auctions.length; }
    
    
    function isActive(uint256 index) public view returns (bool) { return getStatus(index) == Status.active; }
    
    function isFinished(uint256 index) public view returns (bool) { return getStatus(index) == Status.finished; }
    
    
    function getStatus(uint256 index) public view returns (Status) {
        Auction storage auction = auctions[index];
            if (block.timestamp < auction.startTime) {
                return Status.pending;
         } else if (block.timestamp < auction.startTime.add(auction.duration)) {
                return Status.active;
        } else {
                return Status.finished;
        }
    }
    
    function getCurrentBidOwner(uint256 auctionIndex) public view returns (address) { return auctions[auctionIndex].currentBidOwner; }
    
    
    function getCurrentBidAmount(uint256 auctionIndex) public view returns (uint256) { return auctions[auctionIndex].currentBidAmount; }

    function getBidCount(uint256 auctionIndex) public view returns (uint256) { return auctions[auctionIndex].bidCount; }
    
    
    function getWinner(uint256 auctionIndex) public view returns (address) {
        require(isFinished(auctionIndex));
        return auctions[auctionIndex].currentBidOwner;
    }
    
    
    
    function claimAsset(uint256 auctionIndex) public {
        require(isFinished(auctionIndex));
        Auction storage auction = auctions[auctionIndex];

        address winner = getWinner(auctionIndex);
        require(winner == msg.sender);

        asset = IERC271Token(auction.assetAddress);
        // ERC721 asset = ERC721(auction.assetAddress);
        asset.transferFrom(auction.creator, winner, auction.assetId);

        emit Claim(auctionIndex, winner);
    }
    
    function claimEther(uint256 auctionIndex) public {
        require(isFinished(auctionIndex));
        Auction storage auction = auctions[auctionIndex];

        require(auction.creator == msg.sender);
        
        auction.creator.transfer(auction.currentBidAmount);

        emit Claim(auctionIndex, auction.creator);
    }
    
}



// testing
// 1 Deploy erc21 nft contract
// 2 Mint a token 
// 3 deploy auctioncontract
// 4 from nft contract approve auction to transfer token
// 5 owner of NFT - launch an auction - workout timing
// 6  change address in wallets and bid
// 7  claim NFT  
// 8 claim ether
//